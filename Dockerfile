FROM ubuntu:16.04
RUN apt-get update && apt-get install -y ansible sudo && apt-get clean

RUN useradd --create-home user

# Setter opp passordfri sudo til root
RUN echo "user ALL = NOPASSWD: ALL" > /etc/sudoers.d/user

WORKDIR /home/user
USER user
CMD ['bash']

