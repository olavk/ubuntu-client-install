# About

Personal Ansible playbook to install and configure Ubuntu 18.04 client PC.

Contains some standard applications from the default Ubuntu repos, and some extras that are not initially
available (in 16.04 at least), like Slack and Sublime text.

# To use

To use this, you only have to install git (to clone the repo) and ansible to run it.

`sudo apt-get install -y ansible git`

And then run the playbook:

    cd <repodir>
    ansible-playbook main.yaml

The installation should be run as your regular user. It will escalate privileges as necessary, so you need sudo.

If you want to skip some steps, run it with `ansible-playbook --step install.yaml` and choose which you want to execute.

You can also run the separate files in `playbooks/`.

# Testing and development

Can be tested in a Docker container with `cd test` and `make test` if you have make and docker installed.
The test runs in the container as user `user`, with passwordless sudo. 

`make run` gives you a shell, so you can run the install manually, test commands and edit the files,
which are volume mounted under `/home/user/ansible`.

The script should be idempotent, i.e. be able to be run repeatedly without side effects.


# TODO

* personal files in separate dir, so we can have a common base, and then run personal preferences based on $USER
* bash completion blir ikke installert med docker-ce-pakken?

