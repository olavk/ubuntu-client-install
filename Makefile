IMAGE_NAME=ubuntu-client-install
.DEFAULT_GOAL := help

.PHONY: help build clean start shell

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

build: ## Bygger imaget lokalt
	sudo docker build -t ${IMAGE_NAME} .

clean: ## Stopper og sletter alle relaterte containere
	sudo docker stop ${IMAGE_NAME} || true
	sudo docker rm ${IMAGE_NAME}  || true

run: clean ## Starter server med shell inntil videre
	sudo docker run -it --rm -v $(CURDIR):/home/user/ansible --name ${IMAGE_NAME} ${IMAGE_NAME} bash

install: ## Run the script locally (as regular user, not root, but sudo is necessary), installing apps and config
	ansible-playbook --ask-become-pass install.yaml

logs: ## Ser på containerens logg
	sudo docker logs ${IMAGE_NAME}

test: clean ## Bygger container som har ansible og kjører playbook'en
	sudo docker run -it -v $(CURDIR):/home/user/ansible --name ${IMAGE_NAME} ${IMAGE_NAME} ansible-playbook ansible/main.yaml
