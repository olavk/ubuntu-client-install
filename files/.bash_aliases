# User specific aliases and functions

alias dir='ls -l'
alias dira='ls -la'
alias viewcert='openssl x509 -text -in'

#git
alias gd='clear ; git diff -w --color=always | less -r'


#####  SPK  #####
