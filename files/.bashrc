# Custom .bashrc, extends Oh-my-bash

#Global options {{{
export HISTFILESIZE=50000
export HISTSIZE=5000
export HISTCONTROL=ignoredups:ignorespace
#shopt -s checkwinsize


# Filer/kataloger å ignorere ved tab completion
export FIGNORE=.svn:.git

# User specific aliases and functions
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f  ~/.liquidprompt ] ; then
    source ~/.liquidprompt
fi

if [ -f ~/.git-completion.bash ] ; then
    source ~/.git-completion.bash
fi

# Shows last modification date for master and $1 branch
function glm {
    echo master $(git log -u master $2 | grep -m1 Date:)
    echo $1 $(git log -u $1 $2 | grep -m1 Date:)
}

# Søker i Puppet-manifester (*.pp) rekursivt under stående katalog etter angitt parameter
function ppgrep {
    find . \( -name '*.pp' -o -name "*.erb" -o -name "*.yaml" \) | xargs --no-run-if-empty grep -i "$1"
}

# Spytter ut en tilfeldig streng, typisk brukt til å generere passord
# Første parameter angir lengden, f.eks. 'random-string 12'.
function randomstring {
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
}


